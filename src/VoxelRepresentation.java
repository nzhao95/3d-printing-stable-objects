import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import javax.imageio.ImageIO;

import Jcg.geometry.*;
import Jcg.polyhedron.Face;
import Jcg.polyhedron.Polyhedron_3;
import Jcg.polyhedron.Vertex;

/**
 * Voxel representation of a triangulated mesh
 * 
 * @authors Nicolas ZHAO & Benoit GUILLARD
 *
 */

public class VoxelRepresentation {

double scaleFactor=60;		// scaling factor: useful for 3d rendering
MeshViewer view;				// Processing 3d frame (where meshes are rendered)
public Polyhedron_3<Point_3> polyhedron3D; // The original mesh
public double step;			// Quantization step
public Voxel[][][] voxel;	// 3-Dimensional array, storing the raw voxel representations
public int bound_i, bound_j, bound_k; // The size of the 3D voxels' array, along each axis
public double[] bound;		// Stores 6 values : the min and max spatial coordinates of the shape, along the 3 axis

int nVox;					// the number of currently used voxels
Point_3 barycenter;			// the current barycenter of the shape
Point_3 init_barycenter;		// the initial barycenter of the shape
Point_3 support;				// the desired contact point of the shape with the ground

	public VoxelRepresentation(MeshViewer view, double step) {
		this.view=view;
		this.polyhedron3D= view.mesh.polyhedron3D;
		this.step = step;
		Voxel.size = step;
		this.bound = boundingBox(polyhedron3D.vertices);
		this.voxel = initVoxel(this.bound);
		this.support = new Point_3(0,0,0);
	}
	
	// Computes the bounding box of a given collection of vertices,
	// returned as 6 values : the min/max coordinates along each of the 3 axis
	public double[] boundingBox(ArrayList<Vertex<Point_3>> vertices) {
		double[] bound = {Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE};
		for (Vertex<Point_3> v : vertices) {
			Point_3 p = v.getPoint();
			bound[0] = Math.min(bound[0], p.x);
			bound[1] = Math.max(bound[1], p.x);
			bound[2] = Math.min(bound[2], p.y);
			bound[3] = Math.max(bound[3], p.y);
			bound[4] = Math.min(bound[4], p.z);
			bound[5] = Math.max(bound[5], p.z);
		}
		bound[0] -= step;
		bound[2] -= step;
		bound[4] -= step;
		bound[1] += step;
		bound[3] += step;
		bound[5] += step;
		return bound;
	}
	
	// Converts the given spatial coordinates along an axis to
	// the corresponding index in the voxels' 3D array
	// (axe = 0 -> x ; 1 -> y; 2 -> z)
	public int coordToIndice(double coord, int axe) {
		return (int) Math.floor((coord - this.bound[2*axe])/step);
	}
	
	// Converts the given index in the voxels' 3D array to
	// the corresponding spatial coordinates along an axis
	// (axe = 0 -> x ; 1 -> y; 2 -> z)
	public double indiceToCoords(int ind, int axe) {
		return this.bound[2*axe] + ind * step;
	}
	
	// Creates the initial 3D voxels' array
	public Voxel[][][] initVoxel(double[] bound){
		// Compute the size of the array along each axis
		this.bound_i = (int) Math.ceil((bound[1] - bound[0])/step);
		this.bound_j = (int) Math.ceil((bound[3] - bound[2])/step);
		this.bound_k = (int) Math.ceil((bound[5] - bound[4])/step);
		Voxel[][][] ans =  new Voxel[bound_i + 1][bound_j + 1][bound_k + 1];

		// Create the right size array
		System.out.println("The voxels' 3D array is of size : " + Integer.toString(bound_i) + " * " + Integer.toString(bound_j) + " * " + Integer.toString(bound_k));
		
		// Initialize eahc voxel with the right values
		for(int i = 0; i < bound_i + 1; i ++) {
			for(int j = 0; j < bound_j + 1; j ++) {
				for(int k = 0; k < bound_k + 1; k ++) {
					ans[i][j][k] = new Voxel( new Point_3(indiceToCoords(i,0), indiceToCoords(j,1), indiceToCoords(k,2)), i, j, k);
				}
			}			
		}
		return ans;
	}
	
	// Compute the voxels intersecting the outside shell of the mesh :
	public void exteriorVoxel() {
		// Considering each triangle face of the mesh :
		for (Face<Point_3> f : polyhedron3D.facets) {
			// Compute the bounding box of the triangle
			Point_3 p1 = f.getEdge().getVertex().getPoint();
			Point_3 p2 = f.getEdge().getNext().getVertex().getPoint();
			Point_3 p3 = f.getEdge().getOpposite().getVertex().getPoint();
			double[] boundF = {Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE};
			boundF[0] = Math.min(p1.x, Math.min(p2.x, p3.x));
			boundF[1] = Math.max(p1.x, Math.max(p2.x, p3.x));
			boundF[2] = Math.min(p1.y, Math.min(p2.y, p3.y));
			boundF[3] = Math.max(p1.y, Math.max(p2.y, p3.y));
			boundF[4] = Math.min(p1.z, Math.min(p2.z, p3.z));
			boundF[5] = Math.max(p1.z, Math.max(p2.z, p3.z));
			
			// Consider each voxel in this bounding box
			for(int i = coordToIndice(boundF[0], 0); i < coordToIndice(boundF[1], 0) + 1; i ++) {
				for(int j = coordToIndice(boundF[2], 1); j < coordToIndice(boundF[3], 1) + 1; j ++) {
					for(int k = coordToIndice(boundF[4], 2); k < coordToIndice(boundF[5], 2) + 1; k ++) {
						// If the voxel v interesects the triangle,
						// then set v.is_boundary = true (done in the method)
						this.voxel[i][j][k].intersectFace(f);
					}
				}			
			}
		}
	}
	
	// Draw one square face of a voxel, based on its 4 vertices :
	public void drawVoxelFace(VoxelFace f) {
		float s=(float)this.scaleFactor;
		view.vertex((float) (f.points.get(0).getX().doubleValue() * s),(float) (f.points.get(0).getY().doubleValue() * s),(float) (f.points.get(0).getZ().doubleValue() * s));
		view.vertex((float) (f.points.get(1).getX().doubleValue() * s),(float) (f.points.get(1).getY().doubleValue() * s),(float) (f.points.get(1).getZ().doubleValue() * s));
		view.vertex((float) (f.points.get(2).getX().doubleValue() * s),(float) (f.points.get(2).getY().doubleValue() * s),(float) (f.points.get(2).getZ().doubleValue() * s));
		view.vertex((float) (f.points.get(3).getX().doubleValue() * s),(float) (f.points.get(3).getY().doubleValue() * s),(float) (f.points.get(3).getZ().doubleValue() * s));
	}
	
	// Show the voxelization of the outside surface
	public void drawExterior()
	{
		view.beginShape(view.QUAD);
		view.noStroke();
		view.fill(180f, 180f, 250f);
		// Iterate over every voxel :
		for(int i = 0; i < (int) Math.ceil((bound[1] - bound[0])/step); i ++) {
			for(int j = 0; j < (int) Math.ceil((bound[3] - bound[2])/step); j ++) {
				for(int k = 0; k < Math.ceil((bound[5] - bound[4])/step); k ++) {
					// If one voxel belongs to the boundary, show it
					if(this.voxel[i][j][k].is_boundary)
					{
						// Get the voxel's 6 faces
						ArrayList<VoxelFace> v_faces = this.voxel[i][j][k].faces();
						for(VoxelFace f : v_faces)
							this.drawVoxelFace(f);
					}
				}
			}			
		}
		view.strokeWeight(1);
		view.endShape();
	}
	
	// Show the inner voxels (without the shell)
	public void drawInterior()
	{	
		view.beginShape(view.QUAD);
		view.noStroke();
		view.fill(250f, 180f, 180f);
		// Iterate over every voxel :
		for(int i = 0; i < (int) Math.ceil((bound[1] - bound[0])/step); i ++) {
			for(int j = 0; j < (int) Math.ceil((bound[3] - bound[2])/step); j ++) {
				for(int k = 0; k < Math.ceil((bound[5] - bound[4])/step); k ++) {
					// If one voxel is "visible" (ie belongs to the outside shell of the interior domain), show it
					if(this.voxel[i][j][k].visible)
					{
						// Get the voxel's 6 faces
						ArrayList<VoxelFace> v_faces = this.voxel[i][j][k].faces();
						for(VoxelFace f : v_faces)
							this.drawVoxelFace(f);
					}
				}
			}			
		}
		view.strokeWeight(1);
		view.endShape();
		
		// Show the desired standing point as a red sphere
		view.noStroke();
		view.fill(250f, 0f, 0f);
		float s=(float)this.scaleFactor;
		float x1=(float)this.support.getX().doubleValue()*s;
		float y1=(float)this.support.getY().doubleValue()*s;
		float z1=(float)this.support.getZ().doubleValue()*s;
		view.translate(x1, y1, z1);
		view.sphere(s/10f);
		view.translate(-x1, -y1, -z1);
		view.endShape();
		
		// Show 3 axis, intersecting at the current barycenter of the shape
		view.stroke(250f, 0f, 0f);
		view.line(s*this.barycenter.x.floatValue() + (float)1000, s*this.barycenter.y.floatValue(), s*this.barycenter.z.floatValue(), s*this.barycenter.x.floatValue() - (float)1000, s*this.barycenter.y.floatValue(), s*this.barycenter.z.floatValue());
		view.stroke(0f, 250f, 0f);
		view.line(s*this.barycenter.x.floatValue(), s*this.barycenter.y.floatValue() + (float)1000, s*this.barycenter.z.floatValue(), s*this.barycenter.x.floatValue(), s*this.barycenter.y.floatValue() - (float)1000, s*this.barycenter.z.floatValue());
		view.stroke(0f, 0f, 250f);
		view.line(s*this.barycenter.x.floatValue(), s*this.barycenter.y.floatValue(), s*this.barycenter.z.floatValue() + (float)1000, s*this.barycenter.x.floatValue(), s*this.barycenter.y.floatValue(), s*this.barycenter.z.floatValue() - (float)1000);
		
	}
	
	// Fill the inside of the shape with voxels, once the the outside boundary has been computed
	public void fillVoxel() {
		/*
		 * In this function we proceed as such : 
		 * - we start with one exterior voxel, in a corner of the voxels' 3d array
		 * - we do a BFS starting with this voxel, saying each voxel we encounter belongs to the exterior
		 * - the BFS stops expanding when it encounters the boundary
		 * Since the boundary is 'watertight' and delimits 2 clusters, the voxels that have not
		 * been traversed by the BFS belong to the interior !
		 * 
		 */
		
		// The initial number of vertices (that will decrease)
		this.nVox = (bound_i + 1) * (bound_j + 1) * (bound_k + 1) - 1;
		// The q for the BFS :
		Queue<Voxel> q = new LinkedList<Voxel>();
		this.voxel[0][0][0].setValue(false);
		q.add(this.voxel[0][0][0]);
		// Empty the queue
		while (!q.isEmpty()) {
			Voxel v = q.remove();
			// Add each of the current voxel which has not yet been added, and which does not belong to the 
			// boundary : (6 directions, one for each face)
			if (v.i > 0 && !this.voxel[v.i-1][v.j][v.k].is_boundary && this.voxel[v.i-1][v.j][v.k].filled)
			{
				this.voxel[v.i-1][v.j][v.k].setValue(false);
				q.add(this.voxel[v.i-1][v.j][v.k]);
				this.nVox --;
			}
			if (v.j > 0 && !this.voxel[v.i][v.j-1][v.k].is_boundary && this.voxel[v.i][v.j-1][v.k].filled)
			{
				this.voxel[v.i][v.j-1][v.k].setValue(false);
				q.add(this.voxel[v.i][v.j-1][v.k]);
				this.nVox --;
			}
			if (v.k > 0 && !this.voxel[v.i][v.j][v.k-1].is_boundary && this.voxel[v.i][v.j][v.k-1].filled)
			{
				this.voxel[v.i][v.j][v.k-1].setValue(false);
				q.add(this.voxel[v.i][v.j][v.k-1]);
				this.nVox --;
			}
			if (v.i < bound_i && !this.voxel[v.i+1][v.j][v.k].is_boundary && this.voxel[v.i+1][v.j][v.k].filled)
			{
				this.voxel[v.i+1][v.j][v.k].setValue(false);
				q.add(this.voxel[v.i+1][v.j][v.k]);
				this.nVox --;
			}
			if (v.j < bound_j && !this.voxel[v.i][v.j+1][v.k].is_boundary && this.voxel[v.i][v.j+1][v.k].filled)
			{
				this.voxel[v.i][v.j+1][v.k].setValue(false);
				q.add(this.voxel[v.i][v.j+1][v.k]);
				this.nVox --;
			}
			if (v.k < bound_k && !this.voxel[v.i][v.j][v.k+1].is_boundary && this.voxel[v.i][v.j][v.k+1].filled)
			{
				this.voxel[v.i][v.j][v.k+1].setValue(false);
				q.add(this.voxel[v.i][v.j][v.k+1]);
				this.nVox --;
			}
		}
		System.out.println("Number of voxels : " + this.nVox);
	}
	
	// Used to thicken the boundary of the voxelized shape,
	// so that it will finally be printable (without collapsing)
	// t is the desired number of layers that we want to add
	public void thickenBorder(int t) {
		// We first queue each of the voxels belonging to the boundary which are facing inwards
		Queue<Voxel> q = new LinkedList<Voxel>();
		for(int i = 1; i < bound_i - 1; i ++) {
			for(int j = 1; j < bound_j - 1; j ++) {
				for(int k = 1; k < bound_k - 1; k ++) {
					if (this.voxel[i][j][k].is_boundary && (this.voxel[i-1][j][k].filled || this.voxel[i][j-1][k].filled || this.voxel[i][j][k-1].filled
							|| this.voxel[i+1][j][k].filled || this.voxel[i][j+1][k].filled || this.voxel[i][j][k+1].filled)) {
						q.add(this.voxel[i][j][k] );
					}
				}
			}
		}
		// The we dequeue them, and each of their neighbors are added to the boundary
		// The process is repeated t times, by simultaneously filling 
		// a second queue, q2, and then swapping it with q
		Queue<Voxel> q2 = new LinkedList<Voxel>();
		for (int i = 0; i < t; i ++) {
			while (!q.isEmpty()) {
				Voxel v = q.remove();
				// Iterate over each of the 6 neighbors :
				if (v.i > 0 && !this.voxel[v.i-1][v.j][v.k].is_boundary && this.voxel[v.i-1][v.j][v.k].filled)
				{
					this.voxel[v.i-1][v.j][v.k].is_boundary = true;
					q2.add(this.voxel[v.i-1][v.j][v.k]);
				}
				if (v.j > 0 && !this.voxel[v.i][v.j-1][v.k].is_boundary && this.voxel[v.i][v.j-1][v.k].filled)
				{
					this.voxel[v.i][v.j-1][v.k].is_boundary = true;
					q2.add(this.voxel[v.i][v.j-1][v.k]);
				}
				if (v.k > 0 && !this.voxel[v.i][v.j][v.k-1].is_boundary && this.voxel[v.i][v.j][v.k-1].filled)
				{
					this.voxel[v.i][v.j][v.k-1].is_boundary = true;
					q2.add(this.voxel[v.i][v.j][v.k-1]);
				}
				if (v.i < bound_i && !this.voxel[v.i+1][v.j][v.k].is_boundary && this.voxel[v.i+1][v.j][v.k].filled)
				{
					this.voxel[v.i+1][v.j][v.k].is_boundary = true;
					q2.add(this.voxel[v.i+1][v.j][v.k]);
				}
				if (v.j < bound_j && !this.voxel[v.i][v.j+1][v.k].is_boundary && this.voxel[v.i][v.j+1][v.k].filled)
				{
					this.voxel[v.i][v.j+1][v.k].is_boundary = true;
					q2.add(this.voxel[v.i][v.j+1][v.k]);
				}
				if (v.k < bound_k && !this.voxel[v.i][v.j][v.k+1].is_boundary && this.voxel[v.i][v.j][v.k+1].filled)
				{
					this.voxel[v.i][v.j][v.k+1].is_boundary = true;
					q2.add(this.voxel[v.i][v.j][v.k+1]);
				}
			}
			q = new LinkedList(q2);
			q2.clear();
		}
		// Finally, the last queued voxels are the outside shell of the inside voxels.
		// We consequently set them to visible :
		while (!q.isEmpty()) {
			Voxel v = q.remove();
			v.visible = true;	
			v.is_boundary = false;
		}
		
		// At this stage of the process, we are able to compute
		// the desired support point (to one that will touch the ground) (the average of the lowest points)
		// and the initial center of mass
		InnerCarving.supportPoint(this);
		InnerCarving.initialCenterMass(this);
	}
	
	// Compute the priority of one voxel :
	// The priority of a voxel is its distance to the vertical
	// plane that is :
	//  - passing through the desired support point
	//  - perpendicular to to projection of the vector going from the support to the
	//		initial barycenter on the ground plane
	// The higher the priority, the sooner it should be removed to balance the shape
	public double priority(Voxel v) {
		// First : compute the center of mass of the voxel
		Point_3 r = new Point_3(v.points.get(0));
		r.x += step/2;
		r.y += step/2;
		r.z += step/2;
		// Second : compute the inner product between two vectors :
		// - the one going from the desired support to the center of mass of the voxel
		// - the one going from the desired support to the projection of the initial barycenter on the ground plane
		return (r.x - this.support.x)*(this.init_barycenter.x - this.support.x) + (r.z - this.support.z)*(this.init_barycenter.z - this.support.z);
	}
	
	// Compute the global energy :
	// the lower the energy, the more stable the shape
	public double energy() {
		// the energy is the square distance between :
		// - the desired support point
		// - the projection of the barycenter on the ground plane
		Point_2 s = new Point_2(this.support.x, this.support.z);
		Point_2 b = new Point_2(this.barycenter.x, this.barycenter.z);
		return (double) s.squareDistance(b);
	}
	
	// Save the voxelization to .png files, to be used
	// by shapeways to 3D print the shape
	public void toSVXslices()
	{
		// for each shape :
		for(int j = 1; j < bound_j - 1; j ++) {
			try{
				// Create an image of the right size :
	            BufferedImage img = new BufferedImage( 
	                bound_i, bound_k, BufferedImage.TYPE_BYTE_BINARY );
	            File f = new File("slice " + Integer.toString(j) + ".png");
	            // whiten the occupied voxels
				for(int i = 1; i < bound_i - 1; i ++) {
					for(int k = 1; k < bound_k - 1; k ++) {
						if (this.voxel[i][j][k].filled) {
							img.setRGB(i, k, Color.WHITE.getRGB());
						}
					}
				}
				// save the image
				ImageIO.write(img, "PNG", f);
			}
			catch(Exception e){
	            e.printStackTrace();
	        }
		}        
	}
	
}
