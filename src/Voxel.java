import java.util.ArrayList;
import java.util.LinkedList;

import Jcg.geometry.*;
import Jcg.polyhedron.Face;

/**
 * Voxel object, representing one voxel in the 3D grid
 * 
 * @authors Nicolas ZHAO & Benoit GUILLARD
 *
 */

public class Voxel {
	public boolean filled;		// Is the voxel filled ?
	public boolean is_boundary;	// Is the voxel part of the outside boundary of the shape ?
	public static double size;	// The quantization size of the voxel
	public int i, j, k;			// The coordinates of the vixel in the 3D grid
	public ArrayList<Point_3> points;	// The 8 vertices of the voxel
	public boolean visible; 		// Does the voxel belong to the visible part of the interior region ?
	
	
	public Voxel() {
		this.filled = true;
		this.is_boundary = false;
		this.i = 0; this.j = 0; this.k = 0;
	}
	
	public Voxel(Point_3 point, int i, int j, int k) {
		this.filled = true;
		this.is_boundary = false;
		this.i = i; this.j = j; this.k = k;
		// Compute the 8 vertices, based on the first one :
		this.points = new ArrayList<Point_3>();
		points.add(point);
		points.add(new Point_3(point.x + Voxel.size, point.y, point.z));
		points.add(new Point_3(point.x + Voxel.size, point.y + Voxel.size, point.z));
		points.add(new Point_3(point.x, point.y + Voxel.size, point.z));
		points.add(new Point_3(point.x, point.y + Voxel.size, point.z + Voxel.size));
		points.add(new Point_3(point.x + Voxel.size, point.y + Voxel.size, point.z + Voxel.size));
		points.add(new Point_3(point.x + Voxel.size, point.y, point.z + Voxel.size));
		points.add(new Point_3(point.x, point.y, point.z + Voxel.size));
		
	}
	
	public void setValue(boolean b) {
		this.filled = b;
	}

	public void setIndices(int i, int j, int k) {
		this.i = i; this.j = j; this.k = k;
	}
	
	// Returns a list of containing the 6 faces of the Voxel,
	// based on the 8 vertices and their known order
	public ArrayList<VoxelFace> faces(){
		ArrayList<VoxelFace> faces = new ArrayList<VoxelFace>();
		faces.add(new VoxelFace(this.points.get(0), this.points.get(1), this.points.get(6), this.points.get(7), this.points.get(3)));
		faces.add(new VoxelFace(this.points.get(2), this.points.get(3), this.points.get(4), this.points.get(5), this.points.get(1)));
		faces.add(new VoxelFace(this.points.get(1), this.points.get(2), this.points.get(5), this.points.get(6), this.points.get(0)));
		faces.add(new VoxelFace(this.points.get(3), this.points.get(0), this.points.get(7), this.points.get(4), this.points.get(2)));
		faces.add(new VoxelFace(this.points.get(4), this.points.get(5), this.points.get(6), this.points.get(7), this.points.get(3)));
		faces.add(new VoxelFace(this.points.get(0), this.points.get(1), this.points.get(2), this.points.get(3), this.points.get(7)));
		return faces;
	}
	
	// Compute the new intersection polygon, given the previous one and one face of the voxel : 
	public LinkedList<Point_3> computeNewPolygon(VoxelFace f, LinkedList<Point_3> intersectPolygon)
	{
		// Creation of the oriented plane containing the face of the voxel,
		// and facing towards the interior of the voxel
		Plane_3 plane = new Plane_3(f.points.get(0), f.normal);
		
		LinkedList<Point_3> ans = new LinkedList<Point_3>();	// final returned answer
		LinkedList<Point_3> positive = new LinkedList<Point_3>();	// points laying on the positive side of the oriented plane
		LinkedList<Point_3> negative = new LinkedList<Point_3>();	// points laying on the negative side of the oriented plane
		
		for(Point_3 p : intersectPolygon)
		{
			if(plane.hasOnPositiveSide(p))
			{
				positive.add(p);
			}
			else
			{
				negative.add(p);
			}
		}
		// For each couple of points laying on each side of the plane,
		// we compute and add the intersection of the line formed by those 2 points and
		// the currently considered plane
		for(Point_3 p1 : positive)
		{
			for(Point_3 p2 : negative)
			{
				// Thales theorem to compute the intersection of the line
				// and the plane
				double a = Math.abs(p2.getCartesian(f.alignedAxis).doubleValue() - f.points.get(0).getCartesian(f.alignedAxis).doubleValue());
				double b = Math.abs(p1.getCartesian(f.alignedAxis).doubleValue() - f.points.get(0).getCartesian(f.alignedAxis).doubleValue());
				double c = Math.abs(p1.getCartesian(f.alignedAxis).doubleValue() - p2.getCartesian(f.alignedAxis).doubleValue());

				Point_3[] pts = {p2, p1};
				Number[] coeffs = {b/c, a/c};
				
				ans.add(Point_3.linearCombination(pts, coeffs));
			}
		}
		ans.addAll(positive);
		return ans;
	}
	
	// Check if the voxel intersect the face f
	// If so, then it is filled
	public boolean intersectFace(Face<Point_3> f) {
		// Get the 3 vertices of the face's triangle
		Point_3 p1 = f.getEdge().getVertex().getPoint();
		Point_3 p2 = f.getEdge().getNext().getVertex().getPoint();
		Point_3 p3 = f.getEdge().getOpposite().getVertex().getPoint();
		
		// Get the list of the 6 faces of the voxel
		ArrayList<VoxelFace> faces = this.faces();
		
		// Initialize the intersection polygon
		LinkedList<Point_3> intersectPolygon = new LinkedList<Point_3>();
		intersectPolygon.add(p1);
		intersectPolygon.add(p2);
		intersectPolygon.add(p3);
		
		// Iterate over the 6 faces, to compute the intersection polygon
		int faceNr = 0;
		while(faceNr < 6 && !intersectPolygon.isEmpty())
		{
			// Compute the updated intersection polygon :
			intersectPolygon = computeNewPolygon(faces.get(faceNr), intersectPolygon);
			faceNr ++;
			
			// If the intersection polygon becomes empty, then the loop is terminated
		}
		
		// If the intersection contains points,
		// then the voxel is set as belonging to the boundary
		if(!intersectPolygon.isEmpty())
		{
			this.setValue(true);
			this.is_boundary = true;
			return true;
		}
		return false;
	}
	
	
}
