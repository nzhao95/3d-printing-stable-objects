import java.util.Comparator;

/**
 * Custom comparator, to order voxels by decreasing priority
 * 
 * @authors Nicolas ZHAO & Benoit GUILLARD
 *
 */

public class VoxelComparator implements Comparator<Voxel> {
	VoxelRepresentation VR;
	
	VoxelComparator(VoxelRepresentation VR){
		this.VR = VR;
	}
	@Override
	public int compare(Voxel v1, Voxel v2){
		// Compare voxels priority
		double d1 = VR.priority(v1);
		double d2 = VR.priority(v2);

		if (d1 > d2)
			return -1;
		return 1;
	}
}
