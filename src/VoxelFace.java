import java.util.ArrayList;

import Jcg.geometry.*;

/**
 * One face of a voxel
 * 
 * @authors Nicolas ZHAO & Benoit GUILLARD
 *
 */

public class VoxelFace {
	public ArrayList<Point_3> points;	// the for vertices of the square
	public Vector_3 normal;				// the normal vector, pointing towards the interior
	public int alignedAxis;				// the axis along which this face is aligned (0 : x, 1 : y, 2 : z) 
	
	VoxelFace(Point_3 p1, Point_3 p2, Point_3 p3, Point_3 p4, Point_3 b) {
		this.points = new ArrayList<Point_3>();
		this.points.add(p1);
		this.points.add(p2);
		this.points.add(p3);
		this.points.add(p4);
		
		// The fifth point is used to compute the normal vector, pointing
		// towards the interior
		normal = new Vector_3(p1, b);
		
		// Compute the axis along which the face is aligned
		if(normal.y == 0 && normal.z == 0)
			this.alignedAxis = 0;
		else if(normal.x == 0 && normal.z == 0)
			this.alignedAxis = 1;
		else
			this.alignedAxis = 2;
	}
}
