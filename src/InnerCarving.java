import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import Jcg.geometry.Point_2;
import Jcg.geometry.Point_3;

/**
 * Static methods, used to carve the inner space of a voxelization
 * 
 * @authors Nicolas ZHAO & Benoit GUILLARD
 *
 */

public class InnerCarving {
	
	// Compute and set the initial center of mass
	public static void initialCenterMass(VoxelRepresentation VR) {
		// Iterate over all voxel
		// Sum the ones that are filled
		Point_3 c = new Point_3(0,0,0);
		for(int i = 1; i < VR.bound_i - 1; i ++) {
			for(int j = 1; j < VR.bound_j - 1; j ++) {
				for(int k = 1; k < VR.bound_k - 1; k ++) {
					if (VR.voxel[i][j][k].filled) {
						Point_3 p = new Point_3(VR.voxel[i][j][k].points.get(0));
						// Add the center of mass of this voxel to the global center of mass
						c.x += p.x + VR.step/2.;
						c.y += p.y + VR.step/2.;
						c.z += p.z + VR.step/2.;
					}
				}
			}
		}
		// Normalize the global center of mass by the number of filled voxels
		c.multiply(1. / (float) VR.nVox);
		// Set the the current barycenter, and the initial one :
		VR.barycenter = c;
		VR.init_barycenter = new Point_3(c);
	}
	
	// Compute and set the support point of the shape (ie the
	// one that will touch the ground and support the
	// entire structure)
	// This points is found by averaging on the positions of the
	// points that are in the lowest layer of the voxelization
	public static void supportPoint(VoxelRepresentation VR) {
		Point_3 s = new Point_3(0,0,0);
		// Iterate over each layer, starting from the lowest one :
		for(int j =  VR.bound_j - 1; j > 0 ; j --) {
			int n = 0;
			for(int i = 1; i < VR.bound_i - 1; i ++) {
				for(int k = 1; k < VR.bound_k - 1; k ++) {
					// If points are found, add them the the sum of points
					if (VR.voxel[i][j][k].is_boundary) {
						s.x += VR.voxel[i][j][k].points.get(0).x;
						s.y += VR.voxel[i][j][k].points.get(0).y;
						s.z += VR.voxel[i][j][k].points.get(0).z;
						n ++;
					}
				}
			}
			if (s.x != 0 || s.y != 0 || s.z != 0) {
				// Normalize the sum to get the average,
				// and stop going higher in the layers 
				s.x /= n;
				s.y /= n;
				s.z /= n;
				break;
			}
		}
		// To find the true center of mass of the support point :
		s.x += VR.step/2.;
		s.y += VR.step/2.;
		s.z += VR.step/2.;
		VR.support = s;
	}
	
	// Update the current barycenter, given that a voxel has been removed or
	// added.
	// The Point_3 is the center of mass of the Voxel, and 
	// action = 1 if the voxel is added
	// action = -1 otherwise
	public static void updateBar(VoxelRepresentation VR, Point_3 p, int action) {
		assert(action == 1 || action == -1);
		VR.barycenter.x = (VR.barycenter.x * (VR.nVox - action) + action * p.x) / VR.nVox;
		VR.barycenter.y = (VR.barycenter.y * (VR.nVox - action) + action * p.y) / VR.nVox;
		VR.barycenter.z = (VR.barycenter.z * (VR.nVox - action) + action * p.z) / VR.nVox;
	}
	
	// Carve one voxel : delete it from the current representation
	public static void carvePoint(VoxelRepresentation VR, Voxel v) {
		// Remove the voxel from the representation
		v.filled = false;
		// If this voxel was formerly on the visible end of the inner outside, then
		// it is now its neighbors that become visible :
		if (v.visible) {
			v.visible = false;
			// Iterate over all 6 neighboring voxels,
			// those that are filled, and do not belong to the boundary now become visible :
			if (v.i > 0 && !VR.voxel[v.i-1][v.j][v.k].is_boundary && VR.voxel[v.i-1][v.j][v.k].filled)
				VR.voxel[v.i-1][v.j][v.k].visible = true;
			if (v.j > 0 && !VR.voxel[v.i][v.j-1][v.k].is_boundary && VR.voxel[v.i][v.j-1][v.k].filled)
				VR.voxel[v.i][v.j-1][v.k].visible = true;
			if (v.k > 0 && !VR.voxel[v.i][v.j][v.k-1].is_boundary && VR.voxel[v.i][v.j][v.k-1].filled)
				VR.voxel[v.i][v.j][v.k-1].visible = true;
			if (v.i < VR.bound_i && !VR.voxel[v.i+1][v.j][v.k].is_boundary && VR.voxel[v.i+1][v.j][v.k].filled)
				VR.voxel[v.i+1][v.j][v.k].visible = true;
			if (v.j < VR.bound_j && !VR.voxel[v.i][v.j+1][v.k].is_boundary && VR.voxel[v.i][v.j+1][v.k].filled)
				VR.voxel[v.i][v.j+1][v.k].visible = true;
			if (v.k < VR.bound_k && !VR.voxel[v.i][v.j][v.k+1].is_boundary && VR.voxel[v.i][v.j][v.k+1].filled)
				VR.voxel[v.i][v.j][v.k+1].visible = true;
		}
		// The number of voxels decreases
		VR.nVox --;
		// Updating the current barycenter
		Point_3 p = new Point_3(v.points.get(0));
		p.x += VR.step / 2;
		p.y += VR.step / 2;
		p.z += VR.step / 2;
		updateBar(VR, p, -1);
	}
	
	// Un-carve a voxel : refill it in the voxel representation
	public static void uncarvePoint(VoxelRepresentation VR, Voxel v){
		// Reset the voxel to filled and visible
		v.filled = true;
		v.visible = true;
		// Increase the number of voxels
		VR.nVox ++;
		// Update the barycenter
		Point_3 p = new Point_3(v.points.get(0));
		p.x += VR.step / 2;
		p.y += VR.step / 2;
		p.z += VR.step / 2;
		updateBar(VR, p, 1);
	}
	
	// Carve the inner voxels, to bring the barycenter to the closest possible
	// position at the vertical of the support point
	// 		-> balance the whole object
	public static void carve(VoxelRepresentation VR) {
		// Initialize the center of mass and the standing point
		initialCenterMass(VR);
		supportPoint(VR);
		
		// Create a custom priority queue, ordering the voxels by decreasing order
		// of their priority
		// The priority of a voxel is its distance to the vertical
		// plane that is :
		//  - passing through the desired support point
		//  - perpendicular to to projection of the vector going from the support to the
		//		initial barycenter on the ground plane
		// The higher the priority, the sooner it should be removed to balance the shape
		Comparator<Voxel> comparator = new VoxelComparator(VR);	// Custom comparator
		PriorityQueue<Voxel> q = new PriorityQueue<Voxel>(VR.nVox, comparator);
		for(int j = VR.bound_j - 1; j > 0; j --) {
			for(int i = 1; i < VR.bound_i - 1; i ++) {
				for(int k = 1; k < VR.bound_k - 1; k ++) {
					Voxel v = VR.voxel[i][j][k];
					if (v.filled && !v.is_boundary && VR.priority(v) > 0)
						// Adding all inner voxels with positive priority to the queue
						q.add(v);
				}
			}
		}
		
		// Then, dequeue all voxels in their decreasing priority order,
		// and carve them
		// We store the last minimal reached energy in min_energy,
		// and all the voxels that have been carved since in l
		LinkedList<Voxel> l = new LinkedList<Voxel>();
		double min_energy = VR.energy();
		System.out.println("Initial energy : " + Double.toString(min_energy));
		while(!q.isEmpty()) {
			Voxel v = q.remove();
			carvePoint(VR, v);
			double e = VR.energy();
			// If a new minimum is reached, l is cleared
			if (e < min_energy) {
				l.clear();
				min_energy = e;
			}
			else 
				l.add(v);
		}
		System.out.println(l.size());
		// Backtrack to the carving configuration that allowed the smallest energy 
		while (!l.isEmpty()) {
			Voxel v = l.remove();
			uncarvePoint(VR, v);
		}
		System.out.println("Initial energy : " + Double.toString(VR.energy()));
	}
	
}
