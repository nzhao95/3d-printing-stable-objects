import processing.core.*;

import Jcg.geometry.*;
import Jcg.polyhedron.*;

/**
 * A simple 3d viewer for visualizing surface meshes (based on Processing)
 * 
 * @author Luca Castelli Aleardi (INF555, 2012)
 *
 */
public class MeshViewer extends PApplet {
	
	SurfaceMesh mesh; 		// 3d surface mesh
	VoxelRepresentation VR;	// voxel representation of the shape
	double voxelSize = 0.09;	// voxel size (discretization step)
	int renderType=0; 		// choice of type of rendering
	int renderModes=3; 		// number of rendering modes
	
	//String filename="OFF/high_genus_1.off";
	//String filename="OFF/sphere.off";
	//String filename="OFF/cube.off";
	//String filename="OFF/torus_33.off";
	//String filename="OFF/tore.off";
	//String filename="OFF/tri_hedra.off";
	//String filename="OFF/tri_hedra.off";
	String filename="OFF/high_genus_rotate.off";
	//String filename="OFF/letter_a.off";
	//String filename="OFF/star.off";
	//String filename="OFF/tri_triceratops.off";
	
	public void setup() {
		  size(800,600,P3D);
		  ArcBall arcball = new ArcBall(this);
		  
		  this.mesh=new SurfaceMesh(this, filename);
		  this.VR = new VoxelRepresentation(this, voxelSize);
		  VR.scaleFactor = this.mesh.scaleFactor;
	}
		 
		public void draw() {
		  background(0);
		  directionalLight(101, 204, 255, -1, 0, 0);
		  directionalLight(51, 102, 126, 0, -1, 0);
		  directionalLight(51, 102, 126, 0, 0, -1);
		  directionalLight(102, 50, 126, 1, 0, 0);
		  directionalLight(51, 50, 102, 0, 1, 0);
		  directionalLight(51, 50, 102, 0, 0, 1);
		 
		  translate(width/2.f,height/2.f,-1*height/2.f);
		  this.strokeWeight(1);
		  stroke(150,150,150);
		  
		  // Show the original mesh
		  if(this.renderType >= 0)
			  this.mesh.draw(renderType);
		  // OR show the voxelization of the outside surface
		  if(this.renderType == -1)
			  this.VR.drawExterior();
		  // OR show the inner voxels (without the shell)
		  else if(this.renderType == -2)
			  this.VR.drawInterior();
		}
		
		public void keyPressed(){
			  switch(key) {
			    case('v'):case('V'): this.voxelizeSurface(); break;	// Voxellize the outer shape of the mesh
			    case('b'):case('B'): this.fillVoxels(); break;		// Thicken the outer shape, and fill the inside region
			    case('c'):case('C'): this.carve(); break;			// Carve to balance the shape
			    case('s'):case('S'): this.save(); break;				// Save the voxel representation as .png layers, for a 3D printing export
			    case('r'):this.renderType=(this.renderType+1)%this.renderModes; break;
			  }
		}
		
		public void voxelizeSurface() {
			// Voxelize the outer surface of the mesh
			this.VR.exteriorVoxel();
			this.renderType = -1;
		}
		
		public void fillVoxels() {
			// Fill the inside of the shape with voxels
			this.VR.fillVoxel();
			// Thicken the border (for practical building purpose)
			this.VR.thickenBorder(2);
			this.renderType = -2;
		}
		
		public void carve() {
			// Carve the inside of the shape, to MAKE IT STAND
			InnerCarving.carve(this.VR);
			this.renderType = -2;
		}
		
		public void save() {
			// Save the voxel representation as .png layers, for a 3D printing export
			this.VR.toSVXslices();
		}
		
		/**
		 * For running the PApplet as Java application
		 */
		public static void main(String args[]) {
			PApplet.main(new String[] { "MeshViewer" });
		}

}
