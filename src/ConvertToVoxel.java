import java.util.ArrayList;

import Jcg.geometry.*;
import Jcg.polyhedron.Face;
import Jcg.polyhedron.Polyhedron_3;
import Jcg.polyhedron.Vertex;

public class ConvertToVoxel {
	
public Polyhedron_3<Point_3> polyhedron3D;
public double step;
public boolean[][][] voxel; 
public double[] bound;
	
	public ConvertToVoxel(Polyhedron_3<Point_3> polyhedron3D, double step) {
		this.polyhedron3D=polyhedron3D;
		this.step = step; 
		this.bound = boundingBox(polyhedron3D.vertices);
		this.voxel = initVoxel(this.bound);
	}
	
	public double[] boundingBox(ArrayList<Vertex<Point_3>> vertices) {
		double[] bound = {Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE};
		for (Vertex<Point_3> v : vertices) {
			Point_3 p = v.getPoint();
			bound[0] = Math.min(bound[0], p.x);
			bound[1] = Math.max(bound[1], p.x);
			bound[2] = Math.min(bound[2], p.y);
			bound[3] = Math.max(bound[3], p.y);
			bound[4] = Math.min(bound[4], p.z);
			bound[5] = Math.max(bound[5], p.z);
		}
		return bound;
	}
	
	public int coordToIndice(double coord, int axe) {
		return (int) Math.floor((coord - this.bound[axe])/step);
		
	}
	
	
	public boolean[][][] initVoxel(double[] bound){
		return new boolean[(int) Math.ceil((bound[1] - bound[0])/step)][(int) Math.ceil((bound[3] - bound[2])/step)][(int) Math.ceil((bound[5] - bound[4])/step)];
	}
	
	
	public void fillVoxel() {
		for (Face<Point_3> f : polyhedron3D.facets) {
			Point_3 p1 = f.getEdge().getVertex().getPoint();
			Point_3 p2 = f.getEdge().getNext().getVertex().getPoint();
			Point_3 p3 = f.getEdge().getOpposite().getVertex().getPoint();
			double[] boundF = {Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE, Double.MAX_VALUE, Double.MIN_VALUE};
			boundF[0] = Math.min(p1.x, Math.min(p2.x, p3.x));
			boundF[1] = Math.max(p1.x, Math.max(p2.x, p3.x));
			boundF[2] = Math.min(p1.y, Math.min(p2.y, p3.y));
			boundF[3] = Math.max(p1.y, Math.max(p2.y, p3.y));
			boundF[4] = Math.min(p1.z, Math.min(p2.z, p3.z));
			boundF[5] = Math.max(p1.z, Math.max(p2.z, p3.z));
			
			for(int i = coordToIndice(boundF[0], 0); i < coordToIndice(boundF[1], 0); i ++) {
				for(int j = coordToIndice(boundF[2], 1); j < coordToIndice(boundF[3], 1); j ++) {
					for(int k = coordToIndice(boundF[4], 2); k < coordToIndice(boundF[5], 2); k ++) {
						
					}			
				}			
			}
		}
	}
}
